<?php

class UrlController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
    
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
        {
            $url = new Url;
            
            $url->url = Request::get('url');
            $url->description = Request::get('description');
            $url->user_id = Auth::user()->id;

            // Validation and Filtering is sorely needed!!
            // Seriously, I'm a bad person for leaving that out.

            $url->save();
            

            return Response::json(array(
                'status'=>'created',
                'error' => false,
                'urls' => $url->toArray()),
                200
            );
        }
        public function index()
        {
            $urls = Url::where('user_id', Auth::user()->id)->whereNull('deleted_at')->get();

            $data = ( Response::json(array(
                'status'=>'index',
                'error' => false,
                'urls' => $urls->toArray()),
                200
            ));
//            return View::make('include',array('data'=>$data));
            return $data;
        }

        /**
         * Display the specified resource.
         *
         * @param  int  $id
         * @return Response
         */
        public function show($id)
        {

            // Make sure current user owns the requested resource
            $url = Url::where('user_id', Auth::user()->id)
                    ->where('id', $id)
                    ->take(1)
                    ->get();
            $content = array(
                'status'=>'index',
                'hi'=>$_GET,
                'error' => false,
                'urls' => $url->toArray());
            $data =  Response::make($content, '200')->header('Content-Type', 'text/xml');
            return json_encode(View::make('include',array('data'=>$data)));

        }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
        {
            $url = Url::where('user_id', Auth::user()->id)->find($id);

            if ( Request::get('url') )
            {
                $url->url = Request::get('url');
            }

            if ( Request::get('description') )
            {
                $url->description = Request::get('description');
            }

            $url->save();

            return Response::json(array(
                'error' => false,
                'message' => 'url updated'),
                200
            );
        }


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
        {
            
            $url = Url::where('user_id', Auth::user()->id)->find($id);
            $url->delete();
            try {
                $url->delete();
                return Response::json(array(
                'error' => false,
                'message' => 'url deleted'),
                200
                );
            } catch (Exception $ex) {
                echo $ex->getTraceAsString();die;
                
            }
        }


}
