@extends('index')

@section('title', 'Page Title')
@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@stop

@section('content')
    <input type="button" value="click me" data-type="button">
    <input type="hidden" value="testid" id="test">
    <p>This is my body content.</p>
    <script>
        $('[data-type="button"]').click(function(){
            var test = $('#test').val();
            jQuery.ajax({
                type:"Get",
                url:'http://laravel4.local/api/v1/url/1',
                data: {test:test}       ,
                success:function(result){
                    $('p').html(result.urls);
                }
                
            });
        });
    </script>
@stop