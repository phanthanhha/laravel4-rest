<html>
    <head>
        <title>@yeild('title')</title>
        <script src="js/jquery-2.1.4.min.js"></script>
    </head>
    <body>
        @section('sidebar')
         This is the master sidebar 
        @show
        <div class="container">
            @yield('content')
        </div>
    </body>
</html>