<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});
Route::get('test',function(){
    return View::make('include');
});

Route::get('/authtest', array('before' => 'auth.basic', function()
{
    return View::make('hello');
}));

Route::group(array('prefix' => 'api/v1'), function()
{
    Route::resource('url', 'UrlController');
});
//Route::get('api/v1/show/{id}',"UrlController@show");
//Route::get('api/v1/store',"UrlController@store");
//Route::get('api/v1/update/{id}',"UrlController@update");